//[SECTION] Dependencies and Modules
	const Product = require('../models/product');


	module.exports.createProduct = (req,res) => {
		let newProduct = new Product ({
			item:req.body.item,
			description:req.body.description,
			price:req.body.price
			});
		return Product.findOne({item:req.body.item}).then(check => {
				if(check)
				{
					return res.send('Item already exist');
				}
				else 
				 newProduct.save().then((pass, fail) => {
		    		if (pass)
		    		{
		    			console.log(pass);
		    			return res.send('Item successfully registered'); 
				    } else {
				        return res .send('Failed to Register a new account'); 
		      		}; 
		    	});
		  });
		
	};


	module.exports.activeProduct = (req,res) => {
		return Product.find({isActive:true},{ _id : false,isActive:false,createdOn:false,__v:false }).then(result => {
			return res.send(result);
		});
	};

	module.exports.searchProduct = (req,res) => {
		let item = req.body.item;
		return Product.findOne({item:item,isActive:true},{ _id : false,isActive:false,createdOn:false,__v:false }).then(result => {
			if (result)
			{
				return res.send(result);
			}
			else 
			{
				return res.send('Product does not exist');
			};
		});
	};

	module.exports.updateProduct = (req,res) => {
		let item = {item:req.body.item};
		let price = {price:req.body.price};
		return Product.findOneAndUpdate(item,price).then(update	=>{
			if(update)
			{
				return res.send("Product Successfully Updated");
			}
			else
			{
				return 'Failed to update product';
			};
		}); 
	};


	module.exports.archiveProduct = (req,res) =>{
		let item = {item:req.body.item};
		let archive = ({isActive:false});
		return Product.findOneAndUpdate(item,archive).then(update	=>{
			if(update)
			{
				return res.send("Product Successfully Archive");
			}
			else
			{
				return 'Failed to update product';
			};
		}); 
	};

	module.exports.allProduct = (req,res) => {
		return Product.find({}).then(result => {
			return res.send(result);
		});
	};
	module.exports.activateProduct = (req,res) =>{
		let item = {item:req.body.item};
		let activate = ({isActive:true});
		return Product.findOneAndUpdate(item,activate).then(update	=>{
			if(update)
			{
				return res.send("Product Successfully Activate");
			}
			else
			{
				return 'Failed to update product';
			};
		}); 
	};

	module.exports.deleteDatabase = (req,res)=>{
		Product.deleteMany({}).then(deleteAll => {
			if(deleteAll)
			{
			return res.send('Successfully deleted');
			}
			else
			{
				return res.send('NO');
			}
		});
	}