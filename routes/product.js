	const exp = require("express");
  	const controller = require('../controllers/product');
 	const auth = require('../auth');

// destructure verify from auth
  const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 


route.post('/register',verify,verifyAdmin, controller.createProduct)
route.post('/available',verify, controller.activeProduct)
route.get('/explore', verify, controller.searchProduct)
route.put('/update',verify, verifyAdmin, controller.updateProduct)
route.put('/archive',verify, verifyAdmin, controller.archiveProduct)
route.post('/all',verify,verifyAdmin, controller.allProduct)
route.put('/activate',verify,verifyAdmin, controller.activateProduct)
route.delete('/delete',controller.deleteDatabase)
  //[SECTION] Expose Route System
  module.exports = route; 
