	const exp = require("express");
  const controller = require('../controllers/order');
 	const auth = require('../auth');

// destructure verify from auth
  const {verify, verifyAdmin} = auth;
  const route = exp.Router(); 

  route.put('/order', verify, controller.createOrder)
  route.get('/checkout',verify, controller.checkout)
  route.get('/allOrders', verify,verifyAdmin, controller.allOrders)
  route.put('/pay', verify, controller.pay)

//[SECTION] Routing Component
module.exports = route; 