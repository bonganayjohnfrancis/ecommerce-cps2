//[SECTION] Dependencies and Modules
   const mongoose = require('mongoose'); 

//[SECTION] Blueprint Schema

    const orderSchema = new mongoose.Schema({
    	username: {
            type: String,
            required: [true, "Username is Required"]
        },    
        totalAmount: {
    		type: Number,
    		default: 0 
    	},
    	
    	products:
            [{
                item:{
                    type:String
                },
                quantity:{
                    type:Number
                },
                purchasedOn: {
                    type: Date,
                    default: new Date()
        },
            }]
  		
    	
    	
    });


//[SECTION] Model 
  const Order = mongoose.model('Order', orderSchema);
  module.exports = Order;
