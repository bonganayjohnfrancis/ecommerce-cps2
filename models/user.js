//[SECTION] Dependencies and Modules
   const mongoose = require('mongoose'); 

//[SECTION] Blueprint Schema
    const userSchema = new mongoose.Schema({
      username: {
        type: String,
        required: [true, 'Name is Required']
      },
      email: {
        type: String,
        required: [true, 'Email is Required']
      },
      password: {
        type: String,
        required: [true, 'Password in Required']
      },
      isAdmin: {
        type: Boolean,  
        default: false 
      },
      
    });


//[SECTION] Model 
  const User = mongoose.model('User', userSchema);
  module.exports = User;
